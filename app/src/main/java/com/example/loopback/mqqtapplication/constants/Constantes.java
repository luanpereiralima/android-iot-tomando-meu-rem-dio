package com.example.loopback.mqqtapplication.constants;

/**
 * Created by loopback on 18/10/16.
 */

public class Constantes {
    public static String topic        = "android";
    public static String content      = "Message from MqttPublishSample";
    public static int qos             = 2;
    //public static String broker       = "tcp://52.67.171.60:1883";
    public static String broker       = "tcp://192.168.1.8:1883";
    public static String clientId     = "JavaSample";
    //public static final String url = "http://52.67.171.60:3005/horario";
    public static final String url = "http://192.168.1.8:3005/horario";

    public static int SPACE_1 = 1;
    public static int SPACE_2 = 2;
    public static int SPACE_3 = 3;
    public static int SPACE_4 = 4;
    public static int SPACE_5 = 5;
    public static int SPACE_6 = 6;
    public static int SPACE_7 = 7;
    public static int SPACE_8 = 8;
    public static int SPACE_9 = 9;
    public static int SPACE_10 = 10;
    public static int SPACE_11 = 11;
    public static int SPACE_12 = 12;
    public static int SPACE_13 = 13;
}
