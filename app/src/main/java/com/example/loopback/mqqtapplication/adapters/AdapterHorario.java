package com.example.loopback.mqqtapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.loopback.mqqtapplication.R;

import java.util.Date;
import java.util.List;

public class AdapterHorario extends ArrayAdapter<Date> {
        private final Context context;
        private final List<Date> values;

        public AdapterHorario(Context context, List<Date> values) {
                super(context, -1, values);
                this.context = context;
                this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View rowView = inflater.inflate(R.layout.layout_list, parent, false);
                TextView textView = (TextView) rowView.findViewById(R.id.firstLine);
                TextView textView2 = (TextView) rowView.findViewById(R.id.secondLine);
                ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
                textView.setText(values.get(position).getHours()+":"+values.get(position).getMinutes());

                return rowView;
        }
}