package com.example.loopback.mqqtapplication.controller;

import com.example.loopback.mqqtapplication.Horario;
import com.example.loopback.mqqtapplication.constants.Constantes;
import com.example.loopback.mqqtapplication.repositorio.ClientHttp;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class ControladorServer {
	
	private ClientHttp clientHttp;
	private Gson gson;
	private static ControladorServer controladorServer;
	
	public ControladorServer() {
		this.clientHttp = new ClientHttp(new OkHttpClient());
		this.gson = new Gson();
	}
	
	public static ControladorServer getInstance(){
		if(controladorServer !=null)
			return controladorServer;
		
		return controladorServer = new ControladorServer();
	}
	
	public List<Horario> getHorario(){
		
		Request request = new Request.Builder()
			      .url(Constantes.url)
			      .build();
		
		String resultado = clientHttp.getStringBody(request);
		
		if(resultado==null)
			return null;
		
		Type listType = new TypeToken<ArrayList<Horario>>(){}.getType();
		
		return gson.fromJson(resultado, listType);
	}
	
	public boolean save(Horario horario){
		RequestBody requestBody = new FormBody.Builder()
			.add("horario", this.toJson(horario))
			.build();
	
		Request request = new Request.Builder()
		    .url(Constantes.url)
		    .post(requestBody)
		    .build();
		
		return clientHttp.code200(request);
	}
	
	public boolean delete(Horario horario){
		
		RequestBody requestBody = new FormBody.Builder()
			.add("space", horario.getSpace()+"")
			.build();
	
		Request request = new Request.Builder()
		    .url(Constantes.url)
		    .delete(requestBody)
		    .build();
		
		return clientHttp.code200(request);
	}
	
	private String toJson(Horario horario){
		return gson.toJson(horario);
	}
}
