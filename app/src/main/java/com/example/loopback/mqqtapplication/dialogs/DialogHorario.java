package com.example.loopback.mqqtapplication.dialogs;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TimePicker;

import com.example.loopback.mqqtapplication.R;

import org.greenrobot.eventbus.EventBus;

import java.util.Date;
import java.util.TimeZone;

public class DialogHorario extends DialogFragment {
    int mNum;

    public static DialogHorario newInstance(int num) {
        DialogHorario f = new DialogHorario();

        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments().getInt("num");

        // Pick a style based on the num.
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        switch ((mNum-1)%6) {
            case 1: style = DialogFragment.STYLE_NO_TITLE; break;
            case 2: style = DialogFragment.STYLE_NO_FRAME; break;
            case 3: style = DialogFragment.STYLE_NO_INPUT; break;
            case 4: style = DialogFragment.STYLE_NORMAL; break;
            case 5: style = DialogFragment.STYLE_NORMAL; break;
            case 6: style = DialogFragment.STYLE_NO_TITLE; break;
            case 7: style = DialogFragment.STYLE_NO_FRAME; break;
            case 8: style = DialogFragment.STYLE_NORMAL; break;
        }
        switch ((mNum-1)%6) {
            case 4: theme = android.R.style.Theme_Holo; break;
            case 5: theme = android.R.style.Theme_Holo_Light_Dialog; break;
            case 6: theme = android.R.style.Theme_Holo_Light; break;
            case 7: theme = android.R.style.Theme_Holo_Light_Panel; break;
            case 8: theme = android.R.style.Theme_Holo_Light; break;
        }
        setStyle(style, theme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_dialog, container, false);

        final TimePicker picker = (TimePicker) v.findViewById(R.id.timePicker);
        picker.setIs24HourView(true);
        Button b = (Button) v.findViewById(R.id.cadastrar);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimeZone.setDefault(TimeZone.getTimeZone("GTM"));
                Date d = new Date();

                int hour = 0;
                int min = 0;

                int currentApiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentApiVersion > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
                    hour = picker.getHour();
                    min = picker.getMinute();
                } else {
                    hour = picker.getCurrentHour();
                    min = picker.getCurrentMinute();
                }

                d.setHours(hour);
                d.setMinutes(min);
                System.out.println(d);
                EventBus.getDefault().post(d);
                DialogHorario.this.dismiss();
            }
        });

        return v;
    }

}
