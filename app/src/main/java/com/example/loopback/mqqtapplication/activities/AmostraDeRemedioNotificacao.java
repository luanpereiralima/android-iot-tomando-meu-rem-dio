package com.example.loopback.mqqtapplication.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.loopback.mqqtapplication.HorarioNotificacao;
import com.example.loopback.mqqtapplication.R;
import com.example.loopback.mqqtapplication.services.NotificationService;

public class AmostraDeRemedioNotificacao extends AppCompatActivity {

    private HorarioNotificacao horarioNotificacao;
    private NotificationService mBoundService;
    private boolean mIsBound = false;

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mBoundService = ((NotificationService.LocalBinder)service).getService();
            System.out.println("Conectado");
        }

        public void onServiceDisconnected(ComponentName className) {
            mBoundService = null;
            System.out.println("Desconectado");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amostra_de_remedio_notificacao);

        if(!getIntent().hasExtra("horario"))
            finish();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        horarioNotificacao = (HorarioNotificacao) getIntent().getSerializableExtra("horario");

        TextView textoDescricao = (TextView) findViewById(R.id.descricaoR);
        TextView textoHorario = (TextView) findViewById(R.id.horarioR);
        TextView textoPosicao = (TextView) findViewById(R.id.posicaoR);

        setTitle(horarioNotificacao.getNomeRemedio());

        textoDescricao.setText(horarioNotificacao.getDescricao());
        textoHorario.setText("Hora: "+horarioNotificacao.getHorario());
        textoPosicao.setText("Espaço: "+horarioNotificacao.getSpace());
    }

    @Override
    protected void onStart() {
        super.onStart();
        startService(new Intent(this,NotificationService.class));
        doBindService();
    }


    void doBindService() {
        bindService(new Intent(this,
                NotificationService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_amostra_notificacao, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menuPararDeTocar) {
            System.err.println("parando de tocar");
            mBoundService.sendMessage("test", "-1");
        }

        return super.onOptionsItemSelected(item);
    }

}
