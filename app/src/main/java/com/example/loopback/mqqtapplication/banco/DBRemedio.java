package com.example.loopback.mqqtapplication.banco;

import android.content.Context;
import com.example.loopback.mqqtapplication.Horario;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

/**
 * Created by loopback on 02/11/16.
 */

public class DBRemedio {

    public static Horario[] getHorarios(Context context){
        try {
            DB snappydb = DBFactory.open(context);

            Horario [] horarios = null;

            if(snappydb.exists("horarios"))
                horarios = snappydb.getObjectArray("horarios", Horario.class);

            snappydb.close();

            return horarios;

        } catch (SnappydbException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void zerar(Context context){
        try {
            DB snappydb = DBFactory.open(context);
            snappydb.del("horarios");
            snappydb.close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public static boolean addHorario(Context context, Horario horario){
        try {
            DB snappydb = DBFactory.open(context);

            if(horario==null) {
                System.err.println("Horario passado nulo");
                return false;
            }

            Horario [] horarios = null;

            if(snappydb.exists("horarios"))
                horarios = snappydb.getObjectArray("horarios", Horario.class);

            Horario[] buscados;

            if(horarios!=null) {
                buscados = new Horario[horarios.length + 1];
            }else{
                buscados = new Horario[1];
            }

            int count = 0;

            if(horarios!=null)
                for (int i=0; i < horarios.length; i++)
                    if(horarios[i]!=null)
                        if(horarios[i].getSpace() != horario.getSpace())
                            buscados[count++] = horarios[i];

            buscados[count] = horario;

            snappydb.put("horarios", buscados);

            snappydb.close();

            return true;
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean removeHorario(Context context, Horario horario){
        try {
            DB snappydb = DBFactory.open(context);


            if(horario==null) {
                System.err.println("Horario passado nulo");
                return false;
            }

            if(!snappydb.exists("horarios")){
                snappydb.close();
                return true;
            }

            Horario [] horarios = snappydb.getObjectArray("horarios", Horario.class);

            Horario[] buscados = new Horario[horarios.length + 1];

            int count = 0;
            for (int i=0; i < horarios.length; i++)
                if(horarios[i]!=null)
                    if(horarios[i].getSpace() != horario.getSpace())
                        buscados[count++] = horarios[i];

            snappydb.put("horarios", buscados);

            snappydb.close();

            return true;
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return false;
    }

}
