package com.example.loopback.mqqtapplication;

import java.io.Serializable;

/**
 * Created by loopback on 02/11/16.
 */

public class HorarioNotificacao implements Serializable{

    private int space;
    private String nomeRemedio;
    private String descricao;
    private String horario;

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    public String getNomeRemedio() {
        return nomeRemedio;
    }

    public void setNomeRemedio(String nomeRemedio) {
        this.nomeRemedio = nomeRemedio;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
}
