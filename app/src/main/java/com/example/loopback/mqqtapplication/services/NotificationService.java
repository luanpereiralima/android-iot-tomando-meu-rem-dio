package com.example.loopback.mqqtapplication.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.example.loopback.mqqtapplication.HorarioNotificacao;
import com.example.loopback.mqqtapplication.R;
import com.example.loopback.mqqtapplication.activities.AmostraDeRemedioNotificacao;
import com.example.loopback.mqqtapplication.activities.Main;
import com.example.loopback.mqqtapplication.constants.Constantes;
import com.google.gson.Gson;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.Timer;
import java.util.TimerTask;

import static com.example.loopback.mqqtapplication.constants.Constantes.*;

public class NotificationService extends Service {

        private NotificationManager mNM;

        private boolean conectado = false;
        private MqttClient sampleClient;
        private MemoryPersistence persistence = new MemoryPersistence();
        private Timer timer;
        private HorarioNotificacao horarioNotificacao;
        private int NOTIFICATION = 120;

        public class LocalBinder extends Binder {
            public NotificationService getService() {
                return NotificationService.this;
            }
        }

        public boolean sendMessage(String topic, String message){
            MqttMessage msg = new MqttMessage(message.getBytes());
            msg.setQos(Constantes.qos);
            try {
                sampleClient.publish(topic, msg);
                return true;
            } catch (MqttException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        public void onCreate() {
            mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
            timer = new Timer();
            timer.schedule(new ReconectThread(), 0, 30000);
        }

        public void conectar(){
            try {
                sampleClient = new MqttClient(broker, clientId, persistence);
                MqttConnectOptions connOpts = new MqttConnectOptions();

                sampleClient.setCallback(new MqttCallback() {
                    @Override
                    public void connectionLost(Throwable throwable) {
                        throwable.printStackTrace();
                        conectado = false;
                    }

                    @Override
                    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
                        System.out.println("mensagem s: "+ s);
                        System.out.println("mensagem mqttMessage"+ mqttMessage.toString());

                        try{
                            Gson g = new Gson();
                            horarioNotificacao = g.fromJson(mqttMessage.toString(), HorarioNotificacao.class);
                        }catch (Exception e){
                            e.printStackTrace();
                            horarioNotificacao = null;
                        }

                        showNotification(mqttMessage.toString());
                    }

                    @Override
                    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                        System.out.println("EOQ: "+iMqttDeliveryToken.toString());
                    }
                });

                connOpts.setCleanSession(true);
                System.out.println("Connecting to broker: "+broker);
                sampleClient.connect(connOpts);
                System.out.println("Connected");
                System.out.println("Publishing message: "+content);

                sampleClient.subscribe(topic);
                //System.out.println("Message published");
                //sampleClient.disconnect();
                //System.out.println("Disconnected");
                //System.exit(0);
                conectado = true;
            } catch(MqttException me) {
                System.out.println("reason "+me.getReasonCode());
                System.out.println("msg "+me.getMessage());
                System.out.println("loc "+me.getLocalizedMessage());
                System.out.println("cause "+me.getCause());
                System.out.println("excep "+me);
                me.printStackTrace();
                conectado = false;
            }
        }

        private class ReconectThread extends TimerTask{
            @Override
            public void run() {
                if(!conectado)
                    conectar();
            }
        }

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            Log.i("LocalService", "Received start id " + startId + ": " + intent);
            return START_NOT_STICKY;
        }

        @Override
        public void onDestroy() {
            // Cancel the persistent notification.
            mNM.cancel(NOTIFICATION);
            try {
                sampleClient.disconnect();
                conectado = false;
            } catch (MqttException e) {
                e.printStackTrace();
            }
            // Tell the user we stopped.
            Toast.makeText(this, "LocalService", Toast.LENGTH_SHORT).show();
        }

        @Override
        public IBinder onBind(Intent intent) {
            return mBinder;
        }

        // This is the object that receives interactions from clients.  See
        // RemoteService for a more complete example.
        private final IBinder mBinder = new LocalBinder();

        /**
         * Show a notification while this service is running.
         */
        private void showNotification(String mensagem) {
            // In this sample, we'll use the same text for the ticker and the expanded notification
            CharSequence text = "LocalService";

            if(horarioNotificacao==null)
                return;

            Intent intent = new Intent(this, AmostraDeRemedioNotificacao.class);
            intent.putExtra("horario", horarioNotificacao);
            PendingIntent contentIntent;
            contentIntent = PendingIntent.getActivity(this, 0, intent, 0);

            // Set the info for the views that show in the notification panel.
            Notification notification = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.ic_stat_name)  // the status icon
                    .setTicker(horarioNotificacao.getNomeRemedio())  // the status text
                    .setWhen(System.currentTimeMillis())  // the time stamp
                    .setContentTitle("Você precisa tomar o remédio!")  // the label of the entry
                    .setContentText(horarioNotificacao.getNomeRemedio()+"  "+horarioNotificacao.getHorario())  // the contents of the entry
                    .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
                    .setAutoCancel(true)
                    .build();

            // Send the notification.
            mNM.notify(NOTIFICATION, notification);
        }
    }