package com.example.loopback.mqqtapplication;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by loopback on 18/10/16.
 */

public class Horario implements Serializable{
    private List<Date> listaHorarios;
    private int space;
    private String nomeRemedio;
    private String _id;
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public List<Date> getListaHorarios() {
        return listaHorarios;
    }

    public void setListaHorarios(List<Date> listaHorarios) {
        this.listaHorarios = listaHorarios;
    }

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    public String getNomeRemedio() {
        return nomeRemedio;
    }

    public void setNomeRemedio(String nomeRemedio) {
        this.nomeRemedio = nomeRemedio;
    }
}
