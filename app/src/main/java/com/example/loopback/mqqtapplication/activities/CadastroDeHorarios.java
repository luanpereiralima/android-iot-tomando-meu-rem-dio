package com.example.loopback.mqqtapplication.activities;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;

import com.example.loopback.mqqtapplication.Horario;
import com.example.loopback.mqqtapplication.banco.DBRemedio;
import com.example.loopback.mqqtapplication.controller.ControladorServer;
import com.example.loopback.mqqtapplication.dialogs.DialogHorario;
import com.example.loopback.mqqtapplication.adapters.AdapterHorario;
import com.example.loopback.mqqtapplication.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CadastroDeHorarios extends AppCompatActivity {

    private ListView listView;
    private List<Date> list;
    private AdapterHorario adapter;
    private Horario horario;
    private EditText nome;
    private boolean novo;
    private MultiAutoCompleteTextView descricao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!getIntent().hasExtra("horario") && !getIntent().hasExtra("novo"))
            finish();

        setContentView(R.layout.activity_cadastro_de_horarios);

        FloatingActionButton ft = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        nome = (EditText) findViewById(R.id.edtNome);

        descricao = (MultiAutoCompleteTextView) findViewById(R.id.edtDescricao);

        listView = (ListView) findViewById(R.id.listaHorarios);

        novo = getIntent().getBooleanExtra("novo", false);
        horario = (Horario) getIntent().getSerializableExtra("horario");

        if(novo)
            setTitle("Cadastro de Medicamento, Espaço: "+horario.getSpace());
        else
            setTitle("Edição do Medicamento "+horario.getNomeRemedio());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if(novo) {
            list = new ArrayList<>();
            horario.setListaHorarios(list);
        }else{
            list = horario.getListaHorarios();
            nome.setText(horario.getNomeRemedio());
            descricao.setText(horario.getDescricao());
        }

        ft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        adapter = new AdapterHorario(this, list);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final Date item = (Date) parent.getItemAtPosition(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(CadastroDeHorarios.this);

                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        view.animate().setDuration(400).alpha(0)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        list.remove(item);
                                        adapter.notifyDataSetChanged();
                                        view.setAlpha(1);
                                    }
                                });
                    }
                });
                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
                builder.setTitle("Deseja remover esse horário?");

                AlertDialog dialog = builder.create();
                dialog.show();

            }

        });

        if(!novo)
            adapter.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cadastro, menu);

        if(novo)
            menu.removeItem(R.id.menuRemover);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menuCadastro) {

            if(nome.getText().toString().equals("") || descricao.getText().toString().equals("")){
                Snackbar.make(findViewById(android.R.id.content), "Preencha os campos corretamente!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                return true;
            }else{
                horario.setNomeRemedio(nome.getText().toString());
                horario.setDescricao(descricao.getText().toString());
            }


            if (horario.getListaHorarios()!=null && horario.getListaHorarios().size() > 0) {
                new SaveRemedioTask().execute(horario);
            }else{
                Snackbar.make(findViewById(android.R.id.content), "Adicione horários para tomar o remédio", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
            return true;
        }else if(id == R.id.menuRemover){
            AlertDialog.Builder builder = new AlertDialog.Builder(CadastroDeHorarios.this);

            builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    new RemoveRemedioTask().execute(horario);
                }
            });
            builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });
            builder.setTitle("Deseja remover esse Remédio?");

            AlertDialog dialog = builder.create();
            dialog.show();

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Date date) {
        if(!contains(date)) {
            list.add(date);
            adapter.notifyDataSetChanged();
        }
    }

    private boolean contains(Date date){
        for (Date a: list)
            if(a.getHours() == date.getHours() && a.getMinutes() == date.getMinutes())
                return true;
        return false;
    }

    void showDialog() {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = DialogHorario.newInstance(0);
        newFragment.show(ft, "dialog");
    }

    private class SaveRemedioTask extends AsyncTask<Horario, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Horario... params) {
            boolean resultado = ControladorServer.getInstance().save(params[0]);

            if(resultado)
                DBRemedio.addHorario(CadastroDeHorarios.this, params[0]);

            return resultado;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Snackbar.make(findViewById(android.R.id.content), "Cadastrado com sucesso!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                CadastroDeHorarios.this.finish();
            }else {
                Snackbar.make(findViewById(android.R.id.content), "Ocorreu um problema, tente novamente mais tarde", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

    private class RemoveRemedioTask extends AsyncTask<Horario, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Horario... params) {
            boolean resultado = ControladorServer.getInstance().delete(params[0]);

            if(resultado)
                DBRemedio.removeHorario(CadastroDeHorarios.this, params[0]);

            return resultado;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Snackbar.make(findViewById(android.R.id.content), "Removido com sucesso!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                CadastroDeHorarios.this.finish();
            }else {
                Snackbar.make(findViewById(android.R.id.content), "Ocorreu um problema, tente novamente mais tarde", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

}
