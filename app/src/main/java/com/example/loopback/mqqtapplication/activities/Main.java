package com.example.loopback.mqqtapplication.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.example.loopback.mqqtapplication.Horario;
import com.example.loopback.mqqtapplication.banco.DBRemedio;
import com.example.loopback.mqqtapplication.services.NotificationService;
import com.example.loopback.mqqtapplication.R;

public class Main extends AppCompatActivity {

    private NotificationService mBoundService;
    private boolean mIsBound = false;
    private Button[] buttons;
    private Horario[] horarios;

    public void onClickSpace(View view){
        try {
            boolean novo = false;
            int posicao = Integer.parseInt((String) view.getTag());
            System.out.println("tag: "+view.getTag());
            Intent intent = new Intent(this, CadastroDeHorarios.class);
            System.out.println(posicao);
            Horario h = null;

            if(horarios!=null)
                for(int i=0; i < horarios.length; i++)
                    if(horarios[i]!=null)
                        if (horarios[i].getSpace() == posicao)
                            h = horarios[i];

            if(h==null) {
                h = new Horario();
                h.setSpace(posicao);
                novo = true;
            }

            intent.putExtra("horario", h);
            intent.putExtra("novo", novo);
            startActivity(intent);
        }catch (NumberFormatException e){
            e.printStackTrace();
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mBoundService = ((NotificationService.LocalBinder)service).getService();
            System.out.println("Conectado");
        }

        public void onServiceDisconnected(ComponentName className) {
            mBoundService = null;
            System.out.println("Desconectado");
        }
    };

    void doBindService() {
        bindService(new Intent(this,
                NotificationService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        buttons = new Button[11];
        buttons[1] = (Button) findViewById(R.id.button1);
        buttons[2] = (Button) findViewById(R.id.button2);
        buttons[3] = (Button) findViewById(R.id.button3);
        buttons[4] = (Button) findViewById(R.id.button4);
        buttons[5] = (Button) findViewById(R.id.button5);
        buttons[6] = (Button) findViewById(R.id.button6);
        buttons[7] = (Button) findViewById(R.id.button7);
        buttons[8] = (Button) findViewById(R.id.button8);
        buttons[9] = (Button) findViewById(R.id.button9);
        buttons[10] = (Button) findViewById(R.id.button10);
    }

    @Override
    protected void onStart() {
        super.onStart();
        startService(new Intent(this,NotificationService.class));
        doBindService();
     }

    @Override
    protected void onResume() {
        super.onResume();  horarios = DBRemedio.getHorarios(this);

        for (int i=1; i < buttons.length; i++)
            buttons[i].setText(""+i);

        if(horarios!=null) {
            for (int i = 0; i < horarios.length; i++) {
                if(horarios[i]!=null)
                    buttons[horarios[i].getSpace()].setText(horarios[i].getSpace() + ": " + horarios[i].getNomeRemedio().substring(0, horarios[i].getNomeRemedio().length() > 9 ? 8 : horarios[i].getNomeRemedio().length()) + (horarios[i].getNomeRemedio().length() > 9 ? "..." : ""));
            }
        }

    }

}
